# xn--zg8hha

### What? 
An emoji domain duh
### Why? 
Because, 👆👆👆👆
### How?
Thanks to ➝ [Domainoji](https://domainoji.com) && [iwantmyname](https://iwantmyname.com/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
